# Kali-playground

## Intro
This is an simple playground for pentesting with kali

## Startup
```
vagrant up
```

## IPs and Ports
kali --> 9.9.9.9 (ssh: 99)
metasploitable --> 8.8.8.8 (ssh: 88)

## Credentials
kali:
-user: root
-pw: toor

metasploittable:
-user: msfadmin
-pw: msfadmin